from django.urls import path
from .views import api_activity

urlpatterns = [
    path("activity/", api_activity, name="api_activity"),
]
