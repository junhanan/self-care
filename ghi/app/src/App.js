// import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import MainPage from './MainPage';
// import Nav from './Nav';

// function App() {
//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//         <Routes>
//           <Route path="/" element={<MainPage />} />
//         </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }

// export default App;

import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CalendarView from "./CalendarInfo/Calendar.js"

function App() {
  return (
    <BrowserRouter>
    <div className="app">
    <p>see changes</p>
      <div className="container">
        <Routes>
          <Route path="/calendar" element={<CalendarView/>} />
        </Routes>
      </div>
    </div>
    </BrowserRouter>
  );
}

export default App;
