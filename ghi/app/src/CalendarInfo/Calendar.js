import { useState } from 'react';
import Calendar from 'react-calendar';
import "./Calendar.css"

function CalendarView() {
    const [date, setDate] = useState(new Date())
    // const [activities, setActivities] = useState([]);

    // const onClickDay = async (clickedDate) => {
    //     try {
    //         const response = await axios.get()
    //     }
    // }

    return (
        <>
        <div className="calendar">
            <h1 className="header">Calendar View</h1>
            <div className="calendar-container">
                <Calendar onChange={setDate} value={date} />
            </div>
            <div className="text-center">
                Selected date: {date.toDateString()}
            </div>
        </div>
        </>
    )
}

export default CalendarView;
